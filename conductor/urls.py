__author__ = 'keyz'
from django.conf.urls import patterns, url

from conductor import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^/result$', views.results, name='results'),
)