__author__ = 'keyz'
from simplejson import dumps
from copy import deepcopy
from random import randint

from django.template import loader, Context

class Builder():
    PUBLIC_EXPOSED_PORTS = '-p ${{COREOS_PRIVATE_IPV4}}:{0}:{1}'
    PRIVATE_EXPOSED_PORTS = '-p ${{COREOS_PRIVATE_IPV4}}:{0}:{1}'

    def __init__(self, template):
        self._template = deepcopy(template)
        self._units = {}
        self._links = []
        self._from_links = {}
        self._to_links = {}

        self._populate_links(self._template)
        self._populate_units(self._template)
        self._wire_links()

    def _populate_links(self,template):
        #Populate links. Populate [from|to]_links for easy addressing of link enitity
        for link in template['links']:
            if not 'skip_loadbalance' in link:
                link['skip_loadbalance'] = False
            self._links.append(link)
            pos = len(self._links) - 1

            froml = link['from']
            tol = link['to']

            if not froml in self._from_links:
                self._from_links[froml] = []
            self._from_links[froml].append(pos)

            if not tol in self._to_links:
                self._to_links[tol] = []
            self._to_links[tol].append(pos)


    def _populate_units(self,template):
        #Populate units
        for name, unit in template['units'].items():
            if 'count' in unit:
                count = unit['count']
            else:
                count = 1

            unit['name'] = name

            if count == 1:
                self._units[name] = unit
            else:
                for i in range(0,count):
                    newname = "{0}-{1}".format(name,i)
                    #Deep copy the structure, as we want to make independent modifications to all copies
                    self._units[newname] = deepcopy(unit)


    def _wire_links(self):
        #Wire up links
        for newname, unit in self._units.items():
            name = unit['name']
            #FromLinks e.g. Apache -> HAProxy
            if name in self._from_links:
                self._wire_from_links(name, unit)

            #ToLinks e.g. HAProxy -> Apache
            if name in self._to_links:
                self._wire_to_links(name,unit)

            self._units[newname] = unit

    def _wire_from_links(self,name,unit):
        for linknum in self._from_links[name]:
            link = self._links[linknum]

            advertise = {
                "name":name,
                "port":link['port']
            }

            if not 'advertise' in unit:
                unit['advertise'] = []
            unit['advertise'].append(advertise)

    def _wire_to_links(self,name,unit):
        for linknum in self._to_links[name]:
            link = self._links[linknum]

            consume = {
                "port":link['port'],
                "from":link['from'],
                "skip_loadbalance":link['skip_loadbalance']
            }

            if not 'consume' in unit:
                unit['consume'] = []
            unit['consume'].append(consume)

    def render_service_files(self):
        service_files = []
        for name, unit in self._units.items():
            print(name)
            print(dumps(unit))

            to_link = []
            consumes = []

            if 'consume' in unit:
                for consume in unit['consume']:
                    ctx = {
                        'EXPOSED_PORT':randint(40000,50000),
                        'INTERNAL_PORT':consume['port'],
                        'SERVICE_NAME' :consume['from'],
                    }
                    t = loader.get_template('conductor/fleet/consume.template')
                    c = Context(ctx)
                    rendered = t.render(c)
                    svc_name = '{0}-consume-{1}.service'.format(name,consume['from'])
                    service_files.append({
                        'name':svc_name,
                        'content':rendered
                    })

                    if not consume['skip_loadbalance']:
                        to_link.append('{0}:{1}'.format(svc_name,consume['from']))
                    consumes.append(svc_name)

            if 'advertise' in unit:
                for advertise in unit['advertise']:
                    ctx = {
                        'SERVICE_TO_REGISTER':'{}.service'.format(name),
                        'EXPOSED_PORT':advertise['port'],
                        'SERVICE_NAME' :advertise['name']
                    }
                    t = loader.get_template('conductor/fleet/advertise.template')
                    c = Context(ctx)
                    rendered = t.render(c)
                    svc_name = '{0}-advertise-{1}.service'.format(name,advertise['name'])
                    service_files.append({
                        'name':svc_name,
                        'content':rendered
                    })

            exposed_ports = []

            for expose in unit['expose']:
                if 'public' in unit and unit['public']:
                    exposed_ports.append(self.PUBLIC_EXPOSED_PORTS.format(expose,expose))
                else:
                    exposed_ports.append(self.PRIVATE_EXPOSED_PORTS.format(expose,expose))

            exposed_ports = ' '.join(exposed_ports)

            linked_containers = []
            for l in to_link:
                linked_containers.append('--link {0}'.format(l))

            linked_containers = ' '.join(linked_containers)

            ctx = {
                'CONTAINER_NAME':unit['container'],
                'EXPOSED_PORTS':exposed_ports,
                'LINKED_CONTAINERS' :linked_containers,
                'XFLEET'       :True,
                'MACHINEOFLIST':consumes,
                'AFTERSERVICE':consumes
            }
            t = loader.get_template('conductor/fleet/unit.template')
            c = Context(ctx)
            rendered = t.render(c)
            svc_name = '{0}.service'.format(name)
            service_files.append({
                'name':svc_name,
                'content':rendered
            })

        for svc in service_files:
            print(svc['name'])
            print(svc['content'])

        return service_files



