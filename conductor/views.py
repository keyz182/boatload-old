from django.shortcuts import render
from conductor.templater import Builder
from simplejson import loads, dumps

def index(request):
    if request.method == 'POST':
        template = loads(request.POST.get("template",""))

        builder = Builder(template)
        service = builder.render_service_files()

        return render(request, 'conductor/index.html',
                      {
                          'service':service,
                          'template':dumps(template, sort_keys=True, indent=4 * ' ')
                      })
    else:
        return render(request, 'conductor/index.html')


def results(request):
    return render(request, 'conductor/results.html')