from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.http import HttpResponseRedirect

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'boatload.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', lambda r: HttpResponseRedirect('conductor/')),
    url(r'^conductor/', include('conductor.urls', namespace='conductor')),
    url(r'^admin/', include(admin.site.urls)),
)
